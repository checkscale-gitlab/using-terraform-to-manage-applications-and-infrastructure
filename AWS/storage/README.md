terraform init

terraform validate

terraform apply tfplan

terraform destroy -auto-approve -var project_name=la-terraform